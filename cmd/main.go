package cmd

import (
	"chat/examples"
	"chat/pkg/server"
	"sync"
)

func Bootstrap() {
	var wg sync.WaitGroup

	wg.Add(1)
	go server.RunUdp(&wg)

	wg.Add(1)
	examples.RunUdpClient(&wg)

	wg.Wait()
}
