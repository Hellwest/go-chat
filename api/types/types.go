package types

const ProtocolIdSize int = 8
const CommandIdSize int = 4
const PacketIdSize int = 2
const CrcSize int = 4

type Message struct {
	Id         uint
	SenderName string
	Room       string
	Time       string
	Text       string
}

type UDPPacket struct {
	ProtocolId [ProtocolIdSize]byte
	CommandId  [CommandIdSize]byte
	PacketId   [PacketIdSize]byte
	Body       []byte
	Crc        [CrcSize]byte
}

type SendMessagePayload struct {
	MessageId   uint
	SenderName  string
	MessageText string
}

func NewSendMessagePayload(messageId uint, senderName string, messageText string) SendMessagePayload {
	return SendMessagePayload{
		MessageId:   messageId,
		SenderName:  senderName,
		MessageText: messageText,
	}
}
