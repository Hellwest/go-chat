package inputs

type GetMessagesFilter struct {
	Room string
}

type SendMessageInput struct {
	Room string `json:"Room"`
	Text string `json:"Text"`
}
