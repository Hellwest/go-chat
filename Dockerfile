FROM golang:1.16.2-alpine

WORKDIR /app

COPY . /app

EXPOSE 3940/udp

RUN go install

CMD /go/bin/chat
