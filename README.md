# MMO Chat

## Core features

* Room system
* Direct messaging
* Every room corresponds to some region
* You can only chat in the current room
* Message transmission via UDP socket

## Server integration

* Token authentication
* Item linking
