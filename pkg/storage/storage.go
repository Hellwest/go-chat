package storage

import (
	"chat/api/inputs"
	"chat/api/types"
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

var defaultSenderName string = "Anonymous"
var messages []types.Message = []types.Message{{Id: 1, SenderName: defaultSenderName, Room: "eredar", Time: time.Now().UTC().Format(time.RFC3339), Text: "asd"}}
var idSequence uint = uint(len(messages))

func GetMessages(filter inputs.GetMessagesFilter) []types.Message {
	fmt.Printf("Filter: %+v\n", filter)
	room := strings.ToLower(filter.Room)

	if room == "" {
		return []types.Message{}
	}

	var filteredMessages []types.Message

	for _, message := range messages {
		if message.Room == room {
			filteredMessages = append(filteredMessages, message)
		}
	}

	return filteredMessages
}

func SaveMessage(message types.Message) types.Message {
	idSequence++
	message.Id = idSequence
	message.SenderName = defaultSenderName

	messages = append(messages, message)

	fmt.Printf("Current chat content: %+v\n", messages)

	logMessageToFile(message)

	return message
}

func logMessageToFile(message types.Message) {
	file, err := os.OpenFile("msg.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	logger := log.New(file, "", 0)
	logger.Printf("%+v", message)
}
