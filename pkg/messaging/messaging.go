package messaging

import (
	"chat/api/inputs"
	"chat/api/types"
	"chat/pkg/storage"
	"strings"
	"time"
)

func GetMessages(filter inputs.GetMessagesFilter) []types.Message {
	return storage.GetMessages(filter)
}

func SendMessage(input inputs.SendMessageInput) types.SendMessagePayload {
	var message types.Message = types.Message{
		Room: strings.ToLower(input.Room),
		Time: time.Now().UTC().Format(time.RFC3339),
		Text: input.Text,
	}

	message = storage.SaveMessage(message)

	return types.NewSendMessagePayload(message.Id, message.SenderName, message.Text)
}
