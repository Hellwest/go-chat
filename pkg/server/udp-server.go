package server

import (
	"bytes"
	"chat/api/inputs"
	"chat/api/types"
	"chat/pkg/messaging"
	"encoding/binary"
	"fmt"
	"net"
	"sync"
)

const network string = "udp4" // UDP IPv4-only
const port string = ":3940"

const protocolIdBytes byte = 8
const commandIdBytes byte = 4
const packetIdBytes byte = 2
const bodyBytes byte = 237
const crcBytes byte = 4
const packetSize byte = protocolIdBytes + commandIdBytes + packetIdBytes + bodyBytes + crcBytes

func RunUdp(wg *sync.WaitGroup) {
	server, err := net.ResolveUDPAddr(network, port)
	if err != nil {
		fmt.Println(err)
		return
	}

	connection, err := net.ListenUDP(network, server)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer connection.Close()

	for {
		var input []byte = make([]byte, packetSize)
		bytesRead, addr, err := connection.ReadFromUDP(input)
		if err != nil {
			fmt.Printf("Err: %v", err)
			return
		}

		buffer := &bytes.Buffer{}
		if err = binary.Write(buffer, binary.LittleEndian, input[:bytesRead]); err != nil {
			fmt.Printf("Err: %v", err)
			return
		}

		payload := buffer.Bytes()
		packetBodySize := byte(len(payload)) - protocolIdBytes - commandIdBytes - packetIdBytes - crcBytes
		packetProtocolIdEnd := protocolIdBytes
		packetCommandIdEnd := packetProtocolIdEnd + commandIdBytes
		packetPacketIdEnd := packetCommandIdEnd + packetIdBytes
		packetBodyEnd := byte(len(payload)) - crcBytes

		protocolId := payload[:packetProtocolIdEnd]
		commandId := payload[packetProtocolIdEnd : packetProtocolIdEnd+commandIdBytes]
		packetId := payload[packetCommandIdEnd : packetCommandIdEnd+packetIdBytes]
		body := payload[packetPacketIdEnd : packetPacketIdEnd+packetBodySize]
		crc := payload[packetBodyEnd:]
		// fmt.Printf("ProtocolId: %s\n", string(protocolId))
		// fmt.Printf("CommandId: %s\n", string(commandId))
		// fmt.Printf("packetId: %s\n", string(packetId))
		// fmt.Printf("Body: %s\n", string(body))
		// fmt.Printf("Crc: %s\n", string(crc))

		commandId = bytes.TrimRight(commandId, "\000") // Trim empty bytes from [4]byte commandId
		theCommand := string(commandId)

		body = bytes.TrimRight(body, "\000") // Trim empty bytes from []byte body

		switch theCommand {
		case "STOP":
			fmt.Println("Exiting UDP server!")
			wg.Done()
			return

		case "get":
			body = getChatMessages(body)

		case "send":
			body = sendChatMessage(body)
		}

		packetBodySize = byte(len(body))
		packetBodyEnd = packetPacketIdEnd + packetBodySize

		var response []byte = make([]byte, protocolIdBytes+commandIdBytes+packetIdBytes+bodyBytes+crcBytes)
		copy(response[:packetProtocolIdEnd], protocolId)
		copy(response[packetProtocolIdEnd:packetProtocolIdEnd+commandIdBytes], commandId)
		copy(response[packetCommandIdEnd:packetCommandIdEnd+packetIdBytes], packetId)
		copy(response[packetPacketIdEnd:packetPacketIdEnd+packetBodySize], body)
		copy(response[packetBodyEnd:], crc)

		fmt.Printf("Sending response: %s\n", string(response))

		wroteBytes, err := connection.WriteToUDP(response, addr)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Wrote %d bytes\n", wroteBytes)
	}
}

func getChatMessages(body []byte) []byte {
	filter := inputs.GetMessagesFilter{
		Room: string(body),
	}

	messages := messaging.GetMessages(filter)

	return getMessagesBytes(messages)
}

func sendChatMessage(body []byte) []byte {
	input := parseSendMessageInput(body)

	messagePayload := messaging.SendMessage(input)

	return convertResponseToBytes(messagePayload)
}

func getMessagesBytes(messages []types.Message) []byte {
	responseBuffer := &bytes.Buffer{}

	for _, message := range messages {
		if err := binary.Write(responseBuffer, binary.LittleEndian, convertResponseToBytes(message)); err != nil {
			fmt.Printf("Error occurred: %s\n", err)
		}
	}

	return responseBuffer.Bytes()
}

func convertResponseToBytes(r interface{}) []byte {
	responseBuffer := &bytes.Buffer{}
	if err := binary.Write(responseBuffer, binary.LittleEndian, []byte(fmt.Sprintf("%+v", r))); err != nil {
		fmt.Printf("Error occurred: %s\n", err)
	}

	return responseBuffer.Bytes()
}

func parseSendMessageInput(body []byte) inputs.SendMessageInput {
	var roomNameLength int = int(body[0])
	var roomName string = string(body[1 : 1+roomNameLength])

	var messageLength int = int(body[1+roomNameLength])
	var message string = string(body[1+roomNameLength+1 : 1+roomNameLength+1+messageLength])

	return inputs.SendMessageInput{Room: roomName, Text: message}
}
