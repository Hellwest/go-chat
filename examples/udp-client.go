package examples

import (
	"bufio"
	"bytes"
	"chat/api/types"
	"encoding/binary"
	"fmt"
	"net"
	"os"
	"sync"
)

const network string = "udp4" // UDP IPv4-only
const host string = "localhost"
const port string = ":3940"
const address string = host + port

const protocolIdBytes byte = 8
const commandIdBytes byte = 4
const packetIdBytes byte = 2
const bodyBytes byte = 128
const crcBytes byte = 4

func RunUdpClient(wg *sync.WaitGroup) {
	server, _ := net.ResolveUDPAddr(network, address)
	client, err := net.DialUDP(network, nil, server)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer client.Close()

	for {
		commandReader := bufio.NewReader(os.Stdin)
		fmt.Print("Command: ")
		commandInput, err := commandReader.ReadBytes('\n')
		if err != nil {
			fmt.Println(err)
			return
		}
		commandInput = commandInput[:len(commandInput)-1] // trim newline

		var commandId [4]byte
		copy(commandId[:], commandInput[:])

		bodyReader := bufio.NewReader(os.Stdin)
		fmt.Print("Body: ")
		bodyInput, err := bodyReader.ReadBytes('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		bodyInput = bodyInput[:len(bodyInput)-1] // trim newline

		// can't input numbers as bytes from keyboard
		bodyInput = []byte{6, 'e', 'r', 'e', 'd', 'a', 'r', 50, 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'}

		p := types.UDPPacket{
			ProtocolId: [8]byte{'t', 'e', 's', 't'},
			CommandId:  commandId,
			PacketId:   [2]byte{'1'},
			Body:       bodyInput,
			Crc:        [4]byte{'o', 'n', 'e'},
		}

		// so that response doesn't get trimmed midway
		bodySize := byte(237) // byte(len(bodyInput))

		var packet []byte = make([]byte, protocolIdBytes+commandIdBytes+packetIdBytes+bodySize+crcBytes)
		copy(packet[:protocolIdBytes], p.ProtocolId[:])
		copy(packet[protocolIdBytes:protocolIdBytes+commandIdBytes], p.CommandId[:])
		copy(packet[protocolIdBytes+commandIdBytes:protocolIdBytes+commandIdBytes+packetIdBytes], p.PacketId[:])
		copy(packet[protocolIdBytes+commandIdBytes+packetIdBytes:protocolIdBytes+commandIdBytes+packetIdBytes+bodySize], p.Body[:])
		copy(packet[protocolIdBytes+commandIdBytes+packetIdBytes+bodySize:], p.Crc[:])

		// fmt.Printf("Packet in binary view: %b\n", packet)
		// fmt.Printf("Packet in string view: %s\n", string(packet))

		buffer := &bytes.Buffer{}
		if err = binary.Write(buffer, binary.LittleEndian, packet); err != nil {
			fmt.Printf("%v", err)
			return
		}

		_, err = client.Write(buffer.Bytes())
		if string(commandInput) == "STOP" {
			fmt.Println("Exiting UDP client!")
			wg.Done()
			return
		}

		if err != nil {
			fmt.Println(err)
			return
		}

		_, _, err = client.ReadFromUDP(buffer.Bytes())
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Client received packet: %s\n\n", buffer.String())
	}
}
